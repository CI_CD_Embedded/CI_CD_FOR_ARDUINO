#!/bin/sh

echo Compiling blink.ino for Arduino Uno 

echo This is a script for testing compiling .ino files. 

BOARD_NAME="arduino:avr:uno" # --fqbn indicates the board 
FOLDER_NAME="../blink" # blink is the name of the folder, with blink.ino inside

echo $BOARD_NAME
echo $FOLDER_NAME

arduino-cli compile --fqbn $BOARD_NAME $FOLDER_NAME


