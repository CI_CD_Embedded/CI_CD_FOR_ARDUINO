#!/bin/sh

echo Uploading blink.ino for Arduino Uno 

echo This is a script for testing uploading code to arduino uno

BOARD_NAME="arduino:avr:uno" # --fqbn indicates the board 
FOLDER_NAME="../blink" # blink is the name of the folder, with blink.ino inside
PORT_NAME="/dev/ttyACM0" # test the port you are connected to by running: "arduino-cli board list"

echo Board: $BOARD_NAME
echo Folder: $FOLDER_NAME
echo Port: $PORT_NAME

arduino-cli upload -p $PORT_NAME --fqbn $BOARD_NAME $FOLDER_NAME

